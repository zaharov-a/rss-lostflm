<?php

/**
 * 
 */
class Lib_Lostfilm_Downloader {

    /**
     * Одиночка
     * @var \Lib_Lostfilm_Downloader 
     */
    protected static $_self;

    /**
     * Настройки
     * @var array 
     */
    protected $_config = array();

    /**
     * База, где храняться данные о закачках
     * @var SQLite3
     */
    protected $_db;

    /**
     *
     * @var type 
     */
    protected $_curl;
    protected $_siteUrl     = 'https://www.lostfilm.tv';
    protected $_time;
    protected $_curlHeaders = array();

    /**
     * Конструктор
     * @param array $config
     */
    protected function __construct($config) {
        $this->_config = $config;
    }

    /**
     * Получить объект
     * @param array $config
     * @return \Lib_Lostfilm_Downloader
     */
    public static function get($config) {
        if (!self::$_self) {
            self::$_self = new self($config);
        }
        return self::$_self;
    }

    /**
     * Поехали
     * @param int $time
     * @return void
     */
    public function run($time) {
        echo "Старт: " . date('Y-m-d H:i:s', $time) . "\n";

        $this->_time = $time;
        $this->_curl = $this->_getCurl();
        $this->_db   = $this->_getDb();

        $this->_db->query('BEGIN');

        $list = Lib_Lostfilm_Rss::get($this->_config['rssUrl'])
                ->download($this->_time)
                ->getList($this->_config['type']);

        foreach ($list as $row) {
            echo "\"{$row['title']}\" ";
            if ($this->_checkRow($row)) {
                $this->_download($row);
            }
        }

        $this->_db->query('COMMIT');

        $this->_closeCurl();

        echo "Завершение " . (microtime(true) - $this->_time) . "\n";
    }

    public function setCurlHeaders($id, $headers) {
        //$this->_curlHeadersPrev = $this->_curlHeaders;
        $this->_curlHeaders[] = $headers;
        return strlen($headers);
    }

    /**
     * Получить объект базы, если файла базы нет, создать начальную структуру
     * @return \SQLite3
     */
    protected function _getDb() {
        $file = $this->_config['dataDir'] . '/base.db';
        if (is_file($file)) {
            return new SQLite3($file);
        }
        $db = new SQLite3($file);

        $db->query('CREATE TABLE `downloads` (`title`, `pubDate`,`link`, `md5`)');
        $db->query('CREATE UNIQUE INDEX `downloadMd5` ON downloads(`md5`);');

        return $db;
    }

    /**
     * Сгенирировать хеш данных, для исключения повторных скачиваний
     * @param array $row
     * @return string
     */
    public function _generateMd5($row) {
        return md5($row['link'] . $row['title'] . $row['pubDate']);
    }

    /**
     * Проверить не сезон ли или скачивали ли раньше 
     * @param array $row
     * @return boolean
     */
    protected function _checkRow($row) {
        if (!preg_match('/\(S\d\dE\d\d\)$/', $row['title'])) {
            echo "целый сезон\n";
            return false;
        }
        $searchMd5 = $this->_generateMd5($row);
        $result    = $this->_db->query('SELECT * FROM `downloads` WHERE `md5`="' . $searchMd5 . '" LIMIT 1');

        if ($result->fetchArray()) {
            echo "уже скачан\n";
            return false;
        }
        return true;
    }

    /**
     * Инициализировать cUrl
     * @return type
     * @throws Exception
     */
    protected function _getCurl() {
        if (!$curl = curl_init()) {
            throw new Exception('Херня какая-то с cURL');
        }

        curl_setopt($curl, CURLOPT_USERAGENT, $this->_config['userAgent']);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->_config['cookiesFile']); //Из какого файла читать
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->_config['cookiesFile']); //В какой файл записывать

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($curl, CURLOPT_HEADERFUNCTION, array($this, 'setCurlHeaders'));

        if ($this->_config['proxy']['host']) {
            $this->_curlSetProxy($curl);
        }

        return $curl;
    }

    /**
     * Закрыть cUrl
     */
    protected function _closeCurl() {
        if ($this->_curl) {
            curl_close($this->_curl);
        }
    }

    /**
     * Установить прокси
     * @param type $curl
     */
    protected function _curlSetProxy($curl) {
        switch ($this->_config['proxy']['type']) {
            case 'socks5':
                $proxyType = CURLPROXY_SOCKS5;
                break;

            case 'socks4':
                $proxyType = CURLPROXY_SOCKS4;
                break;

            default:
                $proxyType = CURLPROXY_HTTP;
        }
        curl_setopt($curl, CURLOPT_PROXY, $this->_config['proxy']['host'] . ':' . $this->_config['proxy']['port']);
        curl_setopt($curl, CURLOPT_PROXYTYPE, $proxyType);
    }

    /**
     * Скачать
     * @param type $row
     * @throws Exception
     */
    protected function _download($row) {
        echo "$row[link]\n";
//        curl_setopt($this->_curl, CURLOPT_COOKIE, 'lnk_uid=77ddda12c85b81d67a87fa9ff87fc5b2; _ga=GA1.2.1759138841.1474819838; _ym_uid=14748198381068091315; uid=2758368; pass=9b0bdd1e4b1161e7c1f0e59854375608; phpbb2mysql_data=a%3A2%3A%7Bs%3A11%3A%22autologinid%22%3Bs%3A32%3A%225e7b8f6db23b3582011299b39fbebea0%22%3Bs%3A6%3A%22userid%22%3Bs%3A7%3A%222736208%22%3B%7D; lf_session=e13d1196472ae7a51488e172096f5f73; _gat=1; _ym_isad=2; _ym_visorc_5681512=b');
        $details = $this->_curlExec($row['link']);

//        exit('<pre>'.print_r(1, true).'</pre>');
//        $parseUrl = parse_url($row['link']);
//        $params   = array();
//        parse_str($parseUrl['query'], $params);
//        if (!$params['id']) {
//            throw new Exception('Че-то не то с ссылкой');
//        }
//
//        $url     = $this->_siteUrl . '/details.php?id=' . $params['id'];
//        $details = $this->_curlExec($url); //, array(CURLOPT_HEADER => true));
        list($c, $s, $e) = $this->_getCSE($details);

        if (in_array($c, $this->_config['exceptions'])) {
            echo "в исключениях\n";
            return;
        }

//        $nrdr2Url = $this->_siteUrl . '/nrdr2.php?c=' . $c . '&s=' . $s . '&e=' . $e;
        $nrdr2Url = $this->_siteUrl . '/v_search.php?c=' . $c . '&s=' . $s . '&e=' . $e;
        $nrdr2    = $this->_curlExec($nrdr2Url);

        $torrentsPageUrl = preg_replace('/^.+location.replace\(\"(.+)\"\).+$/s', '$1', $nrdr2);

        $torrentsPage = $this->_curlExec($torrentsPageUrl);

        if (!($torrentUrl = $this->_getTorrentUrl($torrentsPage))) {
            echo "Нет версии " . $this->_config['type'] . "\n";
            return;
        }
        //curl_setopt($this->_curl, CURLOPT_VERBOSE, true);

        $torrent         = $this->_curlExec($torrentUrl);
        $torrentFileName = $this->_getTorrentFileName();

        file_put_contents($this->_config['downloadDir'] . '/' . $torrentFileName, $torrent);


        $md5 = $this->_generateMd5($row);

        $this->_db->query("INSERT INTO `downloads`(`title`, `pubDate`,`link`, `md5`)VALUES("
                . "'" . $this->_db->escapeString($row['title']) . "',"
                . "'" . $this->_db->escapeString($row['pubDate']) . "',"
                . "'" . $this->_db->escapeString($row['link']) . "',"
                . "'" . $md5 . "')");
        echo "файл загружен" . (microtime(true) - $this->_time) . "\n";
    }

    /**
     * Получить ссылку на торрент  в зависимости от нужного типа
     * @param type $html
     * @return type
     */
    protected function _getTorrentUrl($html) {
        $paths = array(
            Lib_Lostfilm_Rss::TYPE_SD    => '//div[@class="inner-box--list"]/div[1]/div[@class="inner-box--link main"]/a', //'/html/body/div[2]/div[5]/div[1]/div/table/tr/td[2]/a',
            Lib_Lostfilm_Rss::TYPE_1080P => '//div[@class="inner-box--list"]/div[2]/div[@class="inner-box--link main"]/a', //'/html/body/div[2]/div[5]/div[2]/div/table/tr/td[2]/a',
            Lib_Lostfilm_Rss::TYPE_MP4   => '//div[@class="inner-box--list"]/div[3]/div[@class="inner-box--link main"]/a', //'/html/body/div[2]/div[5]/div[3]/div/table/tr/td[2]/a',
        );

        $path = $paths[$this->_config['type']];

        $doc = new DOMDocument();
        @$doc->loadHTML($html);

        $xpath = new DOMXpath($doc);
        $a     = $xpath
                ->query($path)
                ->item(0);

        return $a->attributes->getNamedItem('href')->textContent;
    }

    /**
     * Выполнить запрос cUrl
     * @param type $url
     * @param type $options
     * @return type
     */
    protected function _curlExec($url, $options = array()) {
        $this->_curlHeaders = array();
        curl_setopt($this->_curl, CURLOPT_URL, $url);
//        curl_setopt($this->_curl, CURLOPT_VERBOSE, true);
        foreach ($options as $key => $value) {
            curl_setopt($this->_curl, $key, $value);
        }
        $data = curl_exec($this->_curl);
        file_put_contents($this->_config['dataDir'] . '/file.html', $data);
        //exit($url);
        return $data;
    }

    /**
     * Получить категорию, сезон, эпизод
     * @param type $html
     * @return type
     */
    protected function _getCSE($html) {
        $doc = new DOMDocument();
        @$doc->loadHTML($html);

        $xpath = new DOMXpath($doc);
        $a     = $xpath
                ->query('//div[@id="left-pane"]//div[@class="external-btn"]')//->query('//div[@id="left-pane"]/div/div/div/div/div[@class="external-btn"]')
                ->item(0);

        $onclick = $a->attributes->getNamedItem('onclick')->textContent;

        $str = preg_replace('/^.+\(\'(.+)\'\).*$/', '$1', $onclick);

        return explode("','", $str);
    }

    protected function _getTorrentFileName() {
        foreach ($this->_curlHeaders as $row) {
            if (false === strpos($row, 'attachment;filename=')) {
                continue;
            }
            $r = explode('"', $row);
            return $r[1];
        }

        return false;
    }

}
