<?php

/**
 * Парсер rss
 */
class Lib_Lostfilm_Rss {

    /**
     * Тип файла sd
     */
    const TYPE_SD = 'sd';

    /**
     * Тип файла 1080
     */
    const TYPE_1080P = '1080p';

    /**
     * Тип файла mp4
     */
    const TYPE_MP4 = 'mp4';

    /**
     * одиночка
     * @var Lib_Lostfilm_Rss 
     */
    protected static $_self;

    /**
     * url файла rss
     * @var string 
     */
    protected $_url;

    /**
     * Список релизов
     * @var array[]
     */
    protected $_list = array();

    /**
     * url файла rss
     * @param string $url
     */
    protected function __construct($url) {
        $this->_url = $url;
    }

    /**
     * Получить объект
     * @param string $url
     * @return Lib_Lostfilm_Rss
     */
    public static function get($url) {
        if (!self::$_self) {
            self::$_self = new self($url);
        }
        return self::$_self;
    }

    /**
     * Скачать rss ленту
     * @param int $time
     * @return \Lib_Lostfilm_Rss
     */
    public function download($time) {
//        $this->_list = array(
//            self::TYPE_SD    => array(),
//            self::TYPE_1080P => array(),
//            self::TYPE_MP4   => array(),
//        );
        $this->_list = array();

        $rss = simplexml_load_file($this->_url);

        echo "Скачали RSS: " . (microtime(true) - $time) . "\n";

        foreach ($rss->channel->item as $row) {
//            $type = $this->_getType($row);
//            $this->_list[$type][] = (array) $row;
            $this->_list[] = (array) $row;
        }

        echo "Обработали RSS: " . ((microtime(true) - $time) / 1000) . "\n";

        return $this;
    }

    /**
     * Получить тип записи
     * @param SimpleXMLElement $row
     * @return string
     */
    protected function _getType($row) {
        $title = trim($row->title);

        switch (1) {
            case preg_match('/\[MP4\][\.\(\)\dSE\s]+$/', $title):
                return self::TYPE_MP4;
            case preg_match('/\[1080p\][\.\(\)\dSE\s]+$/', $title):
                return self::TYPE_1080P;
            default:
                return self::TYPE_SD;
        }
    }

    /**
     * Получить список ссылок по типу
     * @return array
     */
    public function getList() {//$type = self::TYPE_MP4) {
        return $this->_list; //[$type];
    }

}
