<?php

require 'config.php';

$descriptorspec = array(
    0 => array("pipe", "r"), // stdin - канал, из которого дочерний процесс будет читать
    1 => array("pipe", "w"), // stdout - канал, в который дочерний процесс будет записывать 
    2 => array("file", "/tmp/error-output.txt", "a") // stderr - файл для записи
);

$cwd = '/tmp';
$env = array('some_option' => 'aeiou');

$process = proc_open('ssh -l ' . $config['ssh']['login'] . ' ' . $config['ssh']['host'] . ' -D ' . $config['ssh']['port'], $descriptorspec, $pipes, $cwd, $env);

if (!is_resource($process)) {
    exit('Не могу запустить ssh');
}

sleep(2);

require 'index.php';

sleep(2);

proc_close($process);
