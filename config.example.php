<?php

/**
 * Примерный конфиг
 */
$config = array(
    'rssUrl'      => 'http://www.lostfilm.tv/rss.xml',
    'dataDir'     => __DIR__ . '/data',
    'downloadDir' => __DIR__ . '/data',
    'cookiesFile' => __DIR__ . '/data/cookies.txt',
    'userAgent'   => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:48.0) Gecko/20100101 Firefox/48.0',
    'type'        => 'mp4',
    'proxy'       => array(// http://hideme.ru/proxy-list/ - список прокси
        'host' => '127.0.0.1',
        'type' => 'socks5', // варианты: [http, socks4, socks5], socks5 - подходит для тунеля через ssh и для tor
        'port' => '9999',
    ),
    'ssh'         => array(
        'login' => 'user',
        'host'  => 'server.ru',
        'port'  => '9999'
    ),
    'exceptions'  => array(), // список id сериалов, которые не надо скачивать (:
);
