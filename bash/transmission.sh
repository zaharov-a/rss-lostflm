#!/bin/bash
DIR=$(cd $(dirname "$0"); pwd)
. "$DIR/_config.sh"

TORRENT_FILE="${TR_TORRENT_DIR}/${TR_TORRENT_NAME}"
if [ -d "$TORRENT_FILE" ]; then
        ln "${TORRENT_FILE}/${FILE_FILTER}" "${ITUNES_DIR}/"
else
    ext=${TORRENT_FILE##*\.}
    case "$ext" in
        "mp4" ) 
            ln "${TORRENT_FILE}" "${ITUNES_DIR}/"
            ;;
        "mkv" )
            ln "${TORRENT_FILE}" "${CONVERT_DIR}/"            
            bash "$DIR/convert_newstudiotv.sh"
            ;;
    esac
fi

if [ "${USER_EMAIL}" != "" ]; then
    echo "Закачали: ${TORRENT_FILE}"|mail -s "Закачка завершена" "${USER_EMAIL}"
fi
