#!/bin/bash
# конвертировать mkv файлы от newstudio в формат для iTunes

DIR=$(cd $(dirname "$0"); pwd)

. "$DIR/_config.sh"

for show in $DIR/list/*
do
    . "$show/info.sh"
    SHOW_NAME=`basename "$show"`
#    echo $SHOW_NAME/$FILE_SHOW/$FILE_TYPE
    for file in $CONVERT_DIR/$SHOW_NAME*.mkv
    do
        if [ -f "$file" ]
        then
            FILE_NAME=`basename $file`
#echo $FILE_NAME
            SE_INFO=${FILE_NAME/$SHOW_NAME/}
#echo $SE_INFO
            season=${SE_INFO:2:2}
            episode=${SE_INFO:5:2}
        
            NEW_FILE_NAME="/tmp/${SHOW_NAME}_${season}_${episode}.mp4"
#echo $NEW_FILE_NAME
#echo ffmpeg -i "${file}" -codec copy -map 0 "$NEW_FILE_NAME"
            ffmpeg -i "${file}" -codec copy -acodec aac -strict -2 "$NEW_FILE_NAME"
            mp4art --add $show/cover.jpg "$NEW_FILE_NAME"
            mp4tags -i $FILE_TYPE -S "$FILE_SHOW" -s "серия $episode" -n $season -M $episode "$NEW_FILE_NAME"
            mv "$NEW_FILE_NAME" "$ITUNES_DIR/"
            rm "$file"
#exit 0;
            fi
    done
done