<?php
$time = microtime(true);

require 'config.php';
require 'Lib/Lostfilm/Rss.php';
require 'Lib/Lostfilm/Downloader.php';

Lib_Lostfilm_Downloader::get($config)->run($time);